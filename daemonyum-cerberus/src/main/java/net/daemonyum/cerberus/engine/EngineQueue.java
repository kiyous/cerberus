package net.daemonyum.cerberus.engine;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * 
 * @author Kiyous
 *
 */
final class EngineQueue extends LinkedBlockingQueue<EngineRunnable> {
	
	EngineCounter count;
	
	EngineQueue(int queueSize) {
		super(queueSize);
		count = new EngineCounter();
	}	
	
	@Override
	public boolean offer(EngineRunnable runnable, long timeout, TimeUnit unit) throws InterruptedException {
		count.increment();
		return super.offer(runnable, timeout, unit);
	}
	
	EngineCounter getActivity() {
		return count;
	}
	
}
