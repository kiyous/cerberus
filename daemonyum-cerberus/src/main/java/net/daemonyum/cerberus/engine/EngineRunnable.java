package net.daemonyum.cerberus.engine;

import org.apache.log4j.Logger;

import net.daemonyum.cerberus.custom.ActiveProcess;

/**
 * Objet Runnable qui sera traité par les threads
 * @author Kiyous
 */
final class EngineRunnable implements Runnable {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(EngineRunnable.class);

	EngineCounter count;
	ActiveProcess process;

	EngineRunnable(EngineCounter count,ActiveProcess process) {
		this.count = count;
		this.process = process;
	}	

	@Override
	public void run() {
		try {
			process.processing();
		} catch (Exception e) {
			LOGGER.error("Une erreur est survenue dans un traitement de thread");
		}finally {
			// fin de la tache, on se retire;
			count.decrement();
		}
	}

	ActiveProcess getActiveProcess() {
		return process;
	}

}


