package net.daemonyum.cerberus.engine;

/**
 * Exception g�n�r� au sein d'un traitement du moteur
 * @author Kiyous
 *
 */
public class EngineException extends Exception {	
	
	EngineException(String message,Throwable t) {
		super(message,t);
	}

}
