package net.daemonyum.cerberus.engine;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

/**
 * Gestion de la cr�ation et du lancement d'un pool de thread
 * @author Kiyous
 *
 */
final class EngineThreadMaker {
	
	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(EngineThreadMaker.class);	
	
	protected class ThreadFactory implements java.util.concurrent.ThreadFactory {
        
        private int numCreatedThreads = 0;

        public ThreadFactory() {}

        public Thread newThread(Runnable runnable) {
            numCreatedThreads++;
            String threadName = "Processing_Thread-" + numCreatedThreads;
            
            Thread result = new Thread(runnable, threadName);            
            LOGGER.info("Creation du thread : " + threadName);            
            return result;
        }
    }   
	
    private final BlockingQueue<? extends Runnable> blockingQueue;
    private final ThreadPoolExecutor threadPool;
    private final ThreadFactory threadsFactory;    
    
    BlockingQueue<EngineRunnable> getBlockingQueue() {
        return (BlockingQueue<EngineRunnable>) blockingQueue;
    }

    protected ThreadPoolExecutor getPool() {
        return threadPool;
    }    
    
    @SuppressWarnings("unchecked")
	EngineThreadMaker(BlockingQueue<EngineRunnable> queue) {
    	LOGGER.info("instanciation de l'objet "+getClass().getName());
    	blockingQueue = queue;        
        threadsFactory = new ThreadFactory();        
        threadPool =
            new ThreadPoolExecutor(EngineManager.POOLSIZE, EngineManager.POOLSIZE, EngineManager.INACTIVE_TIME_WAITING, TimeUnit.SECONDS,
                (BlockingQueue<Runnable>)blockingQueue, threadsFactory); 
        
        LOGGER.info("Creation du pool d'execution termin� (nombre max de threads : " + EngineManager.POOLSIZE + ")");
    }
    
    void executePool(){
    	LOGGER.debug("Lancement du pool en cours");
    	threadPool.prestartAllCoreThreads();
    	LOGGER.debug("Lancement du pool effectué");
    }    
   
    void stopThreadPool() {
    	LOGGER.debug("Arret du pool en cours");        
        threadPool.shutdown();
        LOGGER.debug("Arret du pool effectué.");        
    }
}
