package net.daemonyum.cerberus.engine;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * Lecture de données dans le fichier de configuration
 * @author Kiyous
 *
 */
final class EngineConfig {

	private static EngineConfig instance;

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(EngineConfig.class);
	static String INACTIVE_TIME_WAITING;
	static String POOL_SIZE;
	static String QUEUE_SIZE;

	static String TIMEOUT;
	private final String INACTIVE_TIME_WAITING_KEY = "net.daemonyum.cerberus.multithreading.engine.inactive_time_waiting";
	private final String POOL_SIZE_KEY             = "net.daemonyum.cerberus.multithreading.engine.pool_size";
	private  Properties prop;
	private final String PROPERTIES_FILE_NAME      = "engine_config.properties";
	private final String QUEUE_SIZE_KEY            = "net.daemonyum.cerberus.multithreading.engine.queue_size";
	private final String TIMEOUT_KEY               = "net.daemonyum.cerberus.multithreading.engine.timeout";

	private EngineConfig() {		
		load();		
	}	

	private String getProperties(String propertyName) {
		return prop.getProperty(propertyName).trim();
	}

	private void load() {		
		try {			
			InputStream in = ClassLoader.getSystemResourceAsStream(PROPERTIES_FILE_NAME);
			prop  = new Properties();
			prop.load(in);			
			in.close();
		}catch(FileNotFoundException e) {
			LOGGER.error("Le fichier de configuration du moteur '"+PROPERTIES_FILE_NAME+"' est introuvable", e);
		}catch(IOException e) {
			LOGGER.error("Erreur [load properties]: ", e);
		}catch(Exception e) {
			LOGGER.error("Une erreur est survenue pendant le chargement de la configuration du moteur", e);
		}

		// tester l'existence des clés
		if(prop.containsKey(QUEUE_SIZE_KEY)) {
			QUEUE_SIZE = getProperties(QUEUE_SIZE_KEY);
		}else {
			LOGGER.error("la propriété "+QUEUE_SIZE_KEY+" n'existe pas dans le fichier "+PROPERTIES_FILE_NAME);
		}
		if(prop.containsKey(POOL_SIZE_KEY)) {
			POOL_SIZE = getProperties(POOL_SIZE_KEY);
		}else {
			LOGGER.error("la propriété "+POOL_SIZE_KEY+" n'existe pas dans le fichier "+PROPERTIES_FILE_NAME);
		}
		if(prop.containsKey(TIMEOUT_KEY)) {
			TIMEOUT = getProperties(TIMEOUT_KEY);
		}else {
			LOGGER.error("la propriété "+TIMEOUT_KEY+" n'existe pas dans le fichier "+PROPERTIES_FILE_NAME);
		}
		if(prop.containsKey(INACTIVE_TIME_WAITING_KEY)) {
			INACTIVE_TIME_WAITING = getProperties(INACTIVE_TIME_WAITING_KEY);
		}else {
			LOGGER.error("la propriété "+INACTIVE_TIME_WAITING_KEY+" n'existe pas dans le fichier "+PROPERTIES_FILE_NAME);
		}
	}

	static EngineConfig getInstance() {
		if(instance == null) {
			instance = new EngineConfig();
		}
		return instance;
	}

}
