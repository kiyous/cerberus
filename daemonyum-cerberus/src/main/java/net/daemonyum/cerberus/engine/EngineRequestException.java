package net.daemonyum.cerberus.engine;

/**
 * 
 * @author Kiyous
 *
 */
public class EngineRequestException extends EngineException {
	
	EngineRequestException(String message, Throwable t) {
		super(message,t);
	}

}
