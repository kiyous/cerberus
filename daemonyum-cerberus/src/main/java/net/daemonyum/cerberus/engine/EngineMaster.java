package net.daemonyum.cerberus.engine;

import org.apache.log4j.Logger;

import net.daemonyum.cerberus.custom.Requester;

/**
 * Englobe la gestion de récupération des données et de démarrage des threads
 * afin de les synchroniser 
 * @author Kiyous
 *
 */
final class EngineMaster {	

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(EngineMaster.class);

	// File d'attente des process 
	private EngineQueue blockingQueue; 
	// Requester moteur
	private EngineRequester engineRequester;    
	// Le pool de threads de traitement
	private EngineThreadMaker engineThreadMaker;

	private int queueSize;    

	EngineMaster(Requester requester) {
		LOGGER.info("instanciation de l'objet "+getClass().getName());
		this.queueSize = EngineManager.QUEUE_SIZE;

		// création de la file d'attente d'envoie des interrogations
		blockingQueue = new EngineQueue(queueSize);    	
		// création du pool de threads de traitement
		engineThreadMaker = new EngineThreadMaker(blockingQueue); 
		// création du sélectionneur de données
		engineRequester = new EngineRequester(blockingQueue,requester);
	}    

	EngineQueue getQueue() {
		return blockingQueue;
	}    

	boolean noActivity(){
		return blockingQueue.getActivity().isNull();
	} 

	boolean shutDown() {
		LOGGER.debug("Debut de methode shutDown");        
		/* Arret du pool de threads */
		engineThreadMaker.stopThreadPool();        
		LOGGER.info("Arret des threads.");        
		LOGGER.debug("Fin de methode shutDown");
		return true;
	}

	void startRequester() throws EngineRequestException, EngineException {
		LOGGER.debug("Debut de methode startRequester"); 
		// lance la recherche ebn base    	
		engineRequester.request();
		LOGGER.debug("Fin de methode startRequester");
	}

	void startThread(){    	
		LOGGER.debug("Debut de methode startThread");    	
		// déclenche les threads
		engineThreadMaker.executePool();           
		LOGGER.debug("Fin de methode startThread");
	}


}

