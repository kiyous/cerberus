package net.daemonyum.cerberus.engine;

/**
 * 
 * @author Kiyous
 *
 */
final class EngineCounter {
	
	private int count;
	
	public EngineCounter() {}
	
	public void increment() {
		count++;
	}
	
	public void decrement() {
		count--;
	}
	
	public boolean isNull() {
		return count == 0;
	}

}
