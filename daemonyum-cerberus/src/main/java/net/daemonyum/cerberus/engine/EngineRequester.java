package net.daemonyum.cerberus.engine;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import net.daemonyum.cerberus.custom.ActiveProcess;
import net.daemonyum.cerberus.custom.Requester;

/**
 * R�cup�re les objets � manipuler et les ins�re dans 
 * la file d'attente (BlockingQueue)
 * @author Kiyous
 *
 */
final class EngineRequester {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(EngineRequester.class);

	private final EngineQueue blockingQueue;
	private Requester requester;

	EngineRequester(EngineQueue blockingQueue, Requester requester){
		LOGGER.info("instanciation de l'objet "+getClass().getName());
		this.blockingQueue = blockingQueue;
		this.requester = requester;				
	}

	public void request() throws EngineRequestException, EngineException {
		LOGGER.debug("Debut de methode request");
		int nbInterroToBeProcessed;
		List<EngineRunnable> engineRunnableList;
		// on récupère une liste  de la table de travail
		List<ActiveProcess> objectToBeProcessed = getDataList();	    

		if (objectToBeProcessed != null && !objectToBeProcessed.isEmpty()) {	
			nbInterroToBeProcessed = objectToBeProcessed.size();
			LOGGER.debug("Le requester  a selectionné "+ nbInterroToBeProcessed +" process a traiter.");

			engineRunnableList = transformActiveProcess(objectToBeProcessed);
			while (!engineRunnableList.isEmpty()) {	
				LOGGER.debug("Tentative d'intégration de "+ nbInterroToBeProcessed +" process");
				// on place les process dans la file d'attente
				engineRunnableList = offerActiveProcess(engineRunnableList);

				if (!engineRunnableList.isEmpty()) {
					nbInterroToBeProcessed = engineRunnableList.size();
					LOGGER.debug(nbInterroToBeProcessed +" process n'ont pas pu etre traite.");	                

					// on vide la liste sinon on risque la boucle infini
					engineRunnableList.clear();
				}else {
					LOGGER.debug("Envoi de "+ nbInterroToBeProcessed +" process avec succes.");
				}
			}
		}else {
			LOGGER.info("Aucun process  à traiter.");
		}
		LOGGER.debug("Fin de methode request");	   
	}

	private List<ActiveProcess> getDataList() throws EngineRequestException {

		List<ActiveProcess> finalList = new ArrayList<ActiveProcess>();			
		List<ActiveProcess> listFromDatabase  = null;			

		try {
			// on crée la liste d'objet
			listFromDatabase  = requester.requesting();					
		} catch (Exception e) {			
			throw new EngineRequestException("Récupération des process impossible",e);						
		}

		if(listFromDatabase != null) {
			// on parse la liste obtenue
			int i = 0;
			for(ActiveProcess process: listFromDatabase) {		    		    		
				// on ajoute le process à la liste qui sera retournée
				process.setNumber(i);
				finalList.add(process);
				i++;
			}
			LOGGER.debug("Fin de methode getDataList: return list");
			return finalList;
		}else {
			LOGGER.debug("Fin de methode getDataList: return null");
			return null;
		}		
	}



	private List<EngineRunnable> offerActiveProcess(List<EngineRunnable> entryToManage) throws EngineException {
		LOGGER.debug("Debut de methode offerActiveProcess");	    
		Iterator<EngineRunnable> it = entryToManage.iterator();    
		try {	    	
			while (it.hasNext()) {	
				EngineRunnable currentRunner = it.next();
				boolean currentInterroSent = false;
				while (!currentInterroSent) {
					// on ajoute l'interrogation à la file d'attente
					currentInterroSent = blockingQueue.offer(currentRunner, EngineManager.TIMEOUT, TimeUnit.SECONDS);	                
				}
				// l'interrogation a bien ete envoye, on la supprime de la liste	            
				it.remove();
				LOGGER.debug("L'élément "+ currentRunner.getActiveProcess().name()+" a ete ajoute a la file de traitement.");	            
			}	       
		}catch(InterruptedException e) {
			// Le thread a été interrompu.	    	
			throw new EngineException("La procédure de mise en file a �t� interrompu. Il existe "+ entryToManage.size() +" process non int�gr�s",e);
		}
		LOGGER.debug("Fin de methode offerActiveProcess");
		return entryToManage;
	}	


	private List<EngineRunnable> transformActiveProcess(List<ActiveProcess> entryToManage){
		LOGGER.debug("Debut de methode transformActiveProcess");
		List<EngineRunnable> engineRunnableList = new ArrayList<EngineRunnable>();
		// transformer la liste de process en liste de runnable
		for(ActiveProcess process: entryToManage) {    	
			EngineRunnable engineRunnable = new EngineRunnable(blockingQueue.getActivity(),process);
			engineRunnableList.add(engineRunnable);
		}
		LOGGER.debug("Fin de methode transformActiveProcess");
		return engineRunnableList;
	}

}
