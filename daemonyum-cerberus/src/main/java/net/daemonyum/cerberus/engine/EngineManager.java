package net.daemonyum.cerberus.engine;

import org.apache.log4j.Logger;

import net.daemonyum.cerberus.custom.Requester;

/**
 * Plus haut niveau du moteur.
 * Initialise les variables de configuration et démarre les tâches
 * @author Kiyous
 *
 */
public final class EngineManager {

	private static final Logger LOGGER = net.daemonyum.hydra.logger.Logger.getLogger(EngineManager.class);

	static int INACTIVE_TIME_WAITING;	
	static int POOLSIZE;
	static int QUEUE_SIZE;
	static int TIMEOUT;

	public EngineManager() {
		LOGGER.info("instanciation de l'objet "+getClass().getName());

		EngineConfig.getInstance();
		QUEUE_SIZE = Integer.parseInt(EngineConfig.QUEUE_SIZE);
		POOLSIZE = Integer.parseInt(EngineConfig.POOL_SIZE);
		TIMEOUT = Integer.parseInt(EngineConfig.TIMEOUT);
		INACTIVE_TIME_WAITING = Integer.parseInt(EngineConfig.INACTIVE_TIME_WAITING);	
	}	

	public void manage(Requester requester) throws EngineRequestException, EngineException {	
		LOGGER.debug("Debut de methode manage");			

		EngineMaster master = null;
		try {
			//instanciation de la tâche de traitement des process
			master = new  EngineMaster(requester);	
			master.startThread();			
			master.startRequester();
		}catch(EngineRequestException e) {			
			master.shutDown();
			throw new EngineRequestException("Requester Object Execution Error",e);
		}catch(Exception e) {			
			master.shutDown();
			throw new EngineException("Unwanted Interruption",e);		
		}finally{
			if(master != null) {
				while(!master.noActivity()) {
					// nothing to do
					// attendre la fin des traitements paralleles
					try {
						Thread.sleep(300);
					} catch (InterruptedException e) {
						LOGGER.warn("", e);
					}
				}
				master.shutDown();
			}
			LOGGER.debug("Fin de methode manage");
		}		
	}
}
