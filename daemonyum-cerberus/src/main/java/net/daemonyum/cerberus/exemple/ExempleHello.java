package net.daemonyum.cerberus.exemple;

import java.util.ArrayList;
import java.util.List;

import net.daemonyum.cerberus.custom.ActiveProcess;
import net.daemonyum.cerberus.custom.Requester;
import net.daemonyum.cerberus.engine.EngineException;
import net.daemonyum.cerberus.engine.EngineManager;

/**
 * Exemple 1 d'utilisation du moteur de Thread.
 * Pour utiliser le moteur de thread, deux choses � faire:
 * 	- H�riter de ActiveProcess
 * 	- Impl�menter Requester
 * L'une des possibilit�s est d'avoir 3 objets different pour effectuer les diff�rentes t�ches.
 * AMHA l'utilisation la plus propre est la cr�ation de classes internes h�ritant ou impl�mentant le n�cessaire
 * car (normalement) l'appel au moteur de thread ne devrait pas �tre appel� de plusieurs endroits different.
 * Apr�s il est possible de n'avoir qu'une seule classe (interne ou pas) qui h�rite et impl�mente
 * respectivement ActiveProcess et Requester afin d'effectuer la cr�ation et le traitement au sein d'un seul objet 
 * @author Kiyous
 *
 */
public class ExempleHello {	
	
	public static void main(String[] arg) {
		ExempleHello hello = new ExempleHello();
		EngineManager manager = new EngineManager();
		
		try {
			/*
			 * Ici on est oblig� d'�crire de mani�re instanceMere.new ClasseInterne()
			 * car on se trouve dans la m�thode main (static).
			 * Dans une m�thode classique il suffit de faire new ClasseInterne().
			 */
			manager.manage(hello.new InternalRequester());
		} catch (EngineException e) {
			e.printStackTrace();
		}
		
	}
	
	
	class InternalActiveProcess extends ActiveProcess {

		@Override
		public String label() {
			return "test_proc";
		}

		@Override
		public void processing() {
			System.out.println("Debut de methode run");		
			System.out.println("TEST PRINT BEFORE");
			System.out.println("*** HELLOWORLD "+name()+" *** ");
			System.out.println("TEST PRINT AFTER");
			System.out.println("Fin de methode run");			
		}		
	}
	
	class InternalRequester implements Requester {

		public List<ActiveProcess> requesting() throws Exception {

			List<ActiveProcess> list = new ArrayList<ActiveProcess>();
			
			for(int i=0;i<100;i++) {
				list.add(new InternalActiveProcess());				
			}
			return list;
		}		
	}
}
