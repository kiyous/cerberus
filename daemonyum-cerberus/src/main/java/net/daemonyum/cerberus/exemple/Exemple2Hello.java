package net.daemonyum.cerberus.exemple;

import java.util.ArrayList;
import java.util.List;

import net.daemonyum.cerberus.custom.ActiveProcess;
import net.daemonyum.cerberus.custom.Requester;
import net.daemonyum.cerberus.engine.EngineException;
import net.daemonyum.cerberus.engine.EngineManager;

/**
 * Exemple 2 d'utilisation du moteur de Thread.
 * Pour utiliser le moteur de thread, deux choses � faire:
 * 	- H�riter de ActiveProcess
 * 	- Impl�menter Requester
 * Une des utilisations possible est de tout faire � partir d'un seul objet.
 * On cr�e donc un objet qui h�rite de ActiveProcess et impl�mente Requester.
 * @author Kiyous
 *
 */
public class Exemple2Hello extends ActiveProcess implements Requester {
	
	@Override
	public String label() {
		return "exemple2";
	}
	
	@Override
	public void processing() {
		/*
		 * Traitement des donn�es.
		 * La m�thode processing() est � l'ActiveProcess ce que le run() est au Thread.
		 * C'est donc cette m�thode qui sera ex�cut�e apr�s le start() des threads.
		 * En exemple un simple HelloWorld 
		 */
		System.out.println("Debut de methode run");		
		System.out.println("TEST PRINT BEFORE");
		System.out.println("*** HELLOWORLD "+name()+" *** ");
		System.out.println("TEST PRINT AFTER");
		System.out.println("Fin de methode run");		
	}
	
	public List<ActiveProcess> requesting() throws Exception {
		/*
		 * Dans la m�thode requesting il faut cr�er les donn�es � fournir aux threads.
		 * C'est ici que l'on peut ex�cuter une requ�te pour cr�er l'objet � partir de 
		 * donn�e en base.
		 * Comme exemple, une simple boucle pour cr�e des objets de type Exemple2Hello 
		 * car il h�rite de ActiveProcess.
		 */
		List<ActiveProcess> list = new ArrayList<ActiveProcess>();
		
		for(int i=0;i<100;i++) {
			list.add(new Exemple2Hello());				
		}
		return list;
	}
	
	public static void main(String[] arg) {
		
		EngineManager manager = new EngineManager();
		
		try {
			manager.manage(new Exemple2Hello());
		} catch (EngineException e) {			
			e.printStackTrace();
		}
		
	}
	
	
	

		
		
	
	
	

		
		
	

}
