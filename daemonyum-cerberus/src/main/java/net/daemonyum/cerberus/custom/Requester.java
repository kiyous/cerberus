package net.daemonyum.cerberus.custom;

import java.util.List;

/**
 * Il faut implémenter cette interface pour pouvoir
 * alimenter le EngineRequester en donnée à traiter
 * @author Kiyous
 *
 */
public interface Requester {

	/**
	 * Cette méthode doit créer les objets qui seront utilisés par les Thread
	 * Cela peut être une requete en base ou tout autre procédé.
	 * L'objet en retour doit implémenter ActiveProcess
	 */
	public List<ActiveProcess> requesting() throws Exception;

}
