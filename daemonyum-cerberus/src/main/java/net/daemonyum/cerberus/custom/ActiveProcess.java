package net.daemonyum.cerberus.custom;

/**
 * Il faut h�riter de cette classe pour traiter des donn�es parallelement
 * Les objets (contenant les donn�es) recup�rer par le Requester seront trait�s par cet objet
 * @author Kiyous
 *
 */
public abstract class ActiveProcess {
	
	private Integer number;
	
	/**
	 * methode qui sera appeler par le run du moteur de thread
	 */
	public abstract void processing();
	
	/**
	 * permettra de suivre la trace du process dans les logs
	 */
	public abstract String label();

	public void setNumber(Integer number) {
		this.number = number;
	}
	
	public Integer getNumber() {
		return number;
	}
	
	public String name() {
		return label()+"-"+String.valueOf(number);
	}
}
